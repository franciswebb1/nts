import os

import numpy as np
import pandas as pd

import project.data_pull as data_pull
import project.spotify_api as spotify_api

out_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'output'))

SHOW = "groove-in-the-heart-w-yuka-mizuhara"
gen_playlist = False


def main(show_name=SHOW):
    cp = spotify_api.CreatePlaylist()

    # Let's check that we have a valid spotify token and ID
    token_check = cp.get_user_profile()
    if token_check:
        print('Spotify token is valid. On y va...\n')
    else:
        print('Whoops, looks like the token is out of date. Visit https://developer.spotify.com/console/get-current-user/'
              ' and click "get token".\n')
        return 1

    if os.path.exists(f'{out_path}/{show_name}_tracks_with_uris.csv'):
        library = pd.read_csv(f'{out_path}/{show_name}_tracks_with_uris.csv')
    else:
        url_list = data_pull.find_show_urls(host_name=show_name)
        library = iterate_shows(url_list=url_list)
        library.drop_duplicates(inplace=True)

        # Add track URIs to the library using the Spotify Api module
        library = cp.add_uris_to_lib(library=library)
        library.to_csv(f'{out_path}/{show_name}_tracks_with_uris.csv')

    # Using the track URIs, lets produce a Spotify playlist for the host of all their tracks
    if gen_playlist:
        cp.gen_playlist_for_nts_host(show_name=show_name, library=library)

    # Now lets add track features to the library, for data analysis and plotting
    if os.path.exists(f'{out_path}/{show_name}_features.csv'):
        show_averages = pd.read_csv(f'{out_path}/{show_name}_features.csv')
    else:
        show_averages = cp.generate_show_features_data(library[['Date', 'track_uri', 'Url']])
        show_averages.index.rename('Date', inplace=True)
        show_averages.to_csv(f'{out_path}/{show_name}_features.csv')
    columns = ["danceability", "energy", "key", "loudness", "mode", "speechiness", "acousticness",
               "instrumentalness", "liveness", "valence", "tempo"]

    #TODO Originally, urls where not added to the features df - this is a temp fix but going forward would
    # be good to have these added in from the start
    add_urls_to_show_feature(show_name=show_name)

    show_average = pd.DataFrame(columns=columns, index=[show_name])
    for col in columns:
        show_average.loc[show_name][col] = show_averages[col].mean()
    return show_average


def iterate_shows(url_list):
    col_names = ['Date', 'Artist', 'Song', 'Url']
    library = pd.DataFrame(columns=col_names)
    for url in url_list:
        ph = data_pull.connectBS(url)
        if ph != "Connection error - 111":
            tracklist = data_pull.findTracklist(ph)
            try:
                date = data_pull.dateSplit(url)
            except:
                date = 'Unknown date'
            if tracklist is not None:
                library = library.append(data_pull.findSongs(tracklist=tracklist, date=date, url=url))
    return library


def scrape_host_urls():
    if os.path.exists(f'{out_path}/host_urls.csv'):
        host_urls = pd.read_csv(f'{out_path}/host_urls.csv')
    else:
        host_urls = data_pull.find_host_urls()
        host_urls = pd.Series(host_urls)
        host_urls = host_urls.str.replace('/shows/', '').tolist()
        pd.Series(host_urls).to_csv(f'{out_path}/host_urls.csv')
    return host_urls


def iterate_host_urls():

    '''
    This method currently takes a lot of time - for each host, we find all their shows, for each show we find all their
    tracks and for each track we call spotify to see if it exists. With all the tracks within spotify we then have to
    call again to retrieve the features from the song. As it stands this takes well over one hour, the code will not
    make unnecessary spotify calls and so you can just click run once your token has been updated and it will effectively
    start searching from where you left off.

    This process is parallelisable. Once the list of hosts have been found, finding shows, tracklists, songs and song
    features can all be run independently. We do risk calling spotify to check if a song exists or retrieve the
    features for the same song more than once but given the diverse nature of the shows and tracks played on NTS, this
    isn't seen as a pressing issue.

    To remove the duplication of effort above we require one global database containing every track played on NTS, whether
    or not it exists on Spotify and what its corresponding features are. I can't be arsed to do that right now, but is
    definitely something for the future! Once we have found all distinct tracks, then the next step of getting features
    is parallelisable - if we parallelise the process of finding tracks, could maybe have clashes of two processes finding
    the same song and trying to write to db?

    It would also be nice to find the variance when calculating the average for a shows features from the tracks. If the
    variance was very low we can say with more confidence the style of music - and so more confidence when recommending
    a show like this - I suppose we can do the same with shows of the host vs the average of host...

    One statistic for each show that would be amazing would be host 'talkiness'.
    '''

    host_urls = scrape_host_urls()
    if os.path.exists(f'{out_path}/show_averages_doesnt_exist.csv'):
        show_averages = pd.read_csv(f'{out_path}/show_averages.csv')
    else:
        columns = ["host", "danceability", "energy", "key", "loudness", "mode", "speechiness", "acousticness",
                   "instrumentalness", "liveness", "valence", "tempo"]
        show_averages = pd.DataFrame(columns=columns)
    for url in host_urls['host_url']:
        show_average = main(show_name=url)
        show_average['host'] = url
        show_averages = show_averages.append(show_average)
        show_averages.to_csv(f'{out_path}/show_averages.csv', index=False)
    return show_averages


def concat_show_features():
    host_urls = scrape_host_urls()
    columns = ["host", "date", "danceability", "energy", "key", "loudness", "mode", "speechiness", "acousticness",
               "instrumentalness", "liveness", "valence", "tempo"]
    combined_shows_df = pd.DataFrame(columns=columns)
    for url in host_urls['host_url']:
        if os.path.exists(f'{out_path}/{url}_features_2.csv'):
            show_features = pd.read_csv(f'{out_path}/{url}_features_2.csv')
            if 'Date' in show_features.columns:
                show_features.rename(columns={"Date": "date"}, inplace=True)
            show_features['host'] = url
            combined_shows_df = pd.concat([combined_shows_df, show_features], ignore_index=True)
    combined_shows_df.to_csv(f'{out_path}/combined_shows_features_with_urls_3.csv', index=False)


def update_urls(show_name):
    url_list = data_pull.find_show_urls(host_name=show_name)
    library = iterate_shows(url_list=url_list)
    library.drop_duplicates(inplace=True)
    library.reset_index(drop=True, inplace=True)
    try:
        old_library = pd.read_csv(f'{out_path}/{show_name}_tracks_with_uris.csv')
        new_library = old_library.join(library['Url'])
        new_library.to_csv(f'{out_path}/{show_name}_tracks_with_uris.csv', index=False)
    except:
        print(f'Error joining URLs for {show_name}')


def add_show_urls():
    host_urls = scrape_host_urls()
    for url in host_urls['host_url']:
        update_urls(show_name=url)


def add_urls_to_show_feature(show_name):
    show_tracks = pd.read_csv(f'{out_path}/{show_name}_tracks_with_uris.csv')
    show_features = pd.read_csv(f'{out_path}/{show_name}_features.csv')

    show_tracks_dates = show_tracks.dropna().drop_duplicates(subset=['Date'])

    show_tracks_dates.reset_index(drop=True, inplace=True)
    show_features.reset_index(drop=True, inplace=True)

    updated_features = show_features.join(show_tracks_dates['Url'])
    updated_features.to_csv(f'{out_path}/{show_name}_features.csv', index=False)


def add_urls_to_show_features():
    host_urls = scrape_host_urls()
    for url in host_urls['host_url']:
        try:
            add_urls_to_show_feature(show_name=url)
        except:
            print(f'Error adding url to show: {url}')


def update_spotify_playlist(show_name):
    # Get all existing show urls
    existing_tracks = pd.read_csv(f'{out_path}/{show_name}_tracks_with_uris.csv')
    shows = np.unique(existing_tracks['Url'])

    # Get all NEW show urls
    all_shows = np.array(data_pull.find_show_urls(host_name=show_name))
    shows_to_check = np.setdiff1d(all_shows, shows)

    # Get all new tracks
    library = iterate_shows(url_list=shows_to_check)
    library.drop_duplicates(inplace=True)

    # Now check the track don't ALREADY exist in our spotify playlist
    cp = spotify_api.CreatePlaylist()
    library = cp.add_uris_to_lib(library=library)
    tracks_to_write = library[~library['Url'].isin(existing_tracks['Url'])].dropna()

    # Find playlist id then write new tracks to that id
    playlist_ids = cp.get_playlist_uris()
    for i in range(len(playlist_ids['items'])):
        if playlist_ids['items'][i]['name'] == show_name:
            playlist_id = playlist_ids['items'][i]['id']
            cp.add_tracks_to_playlist(uris=tracks_to_write['track_uri'], playlist_id=playlist_id)
            number_of_tracks = len(tracks_to_write['track_uri'])
            if number_of_tracks == 0:
                print(f'Failed to find any news tracks this week for {show_name}!')
                return 0
            print(f'{number_of_tracks} racks added to {show_name}')
            return 0
    return 1


def add_percentage_of_used_songs_to_features():
    combined_shows_df = pd.read_csv(f'{out_path}/combined_shows_features_with_urls_3.csv')
    used_rows = pd.DataFrame(columns=['used_rows'])
    host_urls = scrape_host_urls()
    for host in host_urls['host_url']:
        tracks = pd.read_csv(f'{out_path}/{host}_tracks_with_uris.csv')
        shows = tracks['Url'].unique()
        for show in shows:
            total_number_of_tracks = len(tracks[tracks['Url'] == show].index)
            number_of_spotify_tracks = len(tracks[tracks['Url'] == show].dropna().index)
            if total_number_of_tracks != 0:
                percentage = number_of_spotify_tracks / total_number_of_tracks
                used_rows.loc[show] = percentage
    used_rows.reset_index(drop=False, inplace=True)
    used_rows.rename(columns={'index': 'Url'}, inplace=True)
    joined = combined_shows_df.join(used_rows.set_index('Url'), on='Url')
    joined.to_csv(f'{out_path}/combined_shows_features_with_urls_5.csv', index=False)


if __name__ == '__main__':
    add_percentage_of_used_songs_to_features()

