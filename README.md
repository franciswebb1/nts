# README #

## Disclaimer 

#### This isn't here to replace live radio! The code has many intrinsic flaws:
*    Huge amount of tracks played on NTS don't live on Spotify (arguably the best ones)
*    The analysis of shows is based on grouping songs into features then averaging it out. We listen to NTS for diversity of tracks and this fails to recognise this!
*    There's no way to actually prove these recommendations 'work', like a traditional classifying or clustering algorithm
*    We listen to hosts for the craic - DJ style and interraction is COMPLETELY ignored here
*    I still don't really have a clue how to write proper nice code

####

This code scrapes tracklists from NTS (https://www.nts.live/about) show pages and is able to produce spotify playlists and perform some basic analysis on them. 
The analysis allows us to recommend similar shows to existing and new users. Ignore those cluster graphs at the bottom, use KDTree to find new shows to recommend now

### Generating a playlist - how do I get started? ###

* Install all packages found in 'requirements.txt'

* Set up a new authentication token - https://developer.spotify.com/console/get-playlist/.
Once found, update `secrets.py` with your relevant spotify user id and authentication token.

* run `main.py` changing the show_name variable to the show you are interested in. Make sure this corresponds to how it is written in the URL online e.g. for Charlie Bones(https://www.nts.live/shows/the-do-you-breakfast-show) show_name = "the-do-you-breakfast-show". 
  If you wish to make a spotify playlist out the other end, make sure to update the gen_playlist variable found in main.py to be True
  
### Recommendation service - how do I get started? 

* To run the recommendation service, you need to scrape all the track data from NTS residents.

* To do this, run ```iterate_hosts()``` contained within ```main.py```. Make sure to read the word of caution above it though.

* It may also be required to update the data output from this. First you need to run ```add_show_urls()```, ```add_urls_to_show_features()``` and finally ```concat_show_features()```.

* Alternatively, message me and I can provide an existing (probably outdated) file with all the data collection already done.

* If you want new users to rate songs, make sure to update and run ```introduction_tracks.py``` (REQUIRES SPOTIFY TOKEN)

* Run ```main.py``` within recommender.py and you should be up and running!

### To Do

* Front end

* Variance and percentage of songs used for show average

* Grow ```ARTISTS_BIG``` for dynamic recommendation artists - the more the merrier

Do!!You!!! generated playlist - soon to be updated weekly (code is there ready to go)
![Scheme](images/spotify_playlist.png)

Investigation of clustered behaviour - consistent patterns indicates ml could be useful (was very relieved after running this)
![Scheme](images/clustered_regions.png)
![Scheme](images/all_clusters_comparisons.png)

Example output
![Scheme](images/charlie-bones-2.png)

For more recommendation examples visit https://docs.google.com/spreadsheets/d/1m94nSxwlZ9Rne1KKtVTRUgCd9SJ4Pfx4jKkxmHnSsLI/edit?usp=sharing