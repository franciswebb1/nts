import pandas as pd 


def countByArtist(library):
    return library[['Artist']].Artist.value_counts()


def countByDay(library):
    return library[['Date']].Date.value_counts()


def dataFilter(library, minCutOff):
    return library[library >= minCutOff]
