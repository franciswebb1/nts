import unittest

import numpy as np
import project.recommender as recommender


class TestRecommender(unittest.TestCase):
    @unittest.skip
    def testSplit2DArrayByX(self):
        x = np.array([[1, 2], [5, 6], [3, 4], [1, 2]])
        x1_check = np.array([[1, 2], [1, 2]])
        x2_check = np.array([[5, 6], [3, 4]])
        (x1, x2) = recommender.split(x=x, d=0)
        self.assertEqual((x1_check, x2_check), (x1, x2))


if __name__ == '__main__':
    unittest.main()

