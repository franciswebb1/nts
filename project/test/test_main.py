import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import project.data_pull as data_pull
import project.data_handler as data_handler
import project.data_grapher as data_grapher
import pandas as pd


class TestDataPuller(unittest.TestCase):
    # static
    url = 'https://www.nts.live/shows/the-do-you-breakfast-show/episodes/charlie-bones-w-jack-rollo-2nd-dec-2019'
    
    def testConnectionValidUrl(self):
        a = data_pull.connectBS(self.url)
        self.assertNotEqual(a, "Connection error - 111")

    def testConnectionInvalidUrl(self):
        url = 'aaa'
        a = data_pull.connectBS(url)
        self.assertEqual(a, "Connection error - 111")

    def testDateSplit(self):
        url = 'https://www.nts.live/shows/the-do-you-breakfast-show/episodes/charlie-bones-w-jack-rollo-2nd-dec-2019'
        date = data_pull.dateSplit(url)
        dateTest = '2nd-december-2019'
        self.assertEqual(date, dateTest)


class TestDataHandler(unittest.TestCase):
    # static
    library = {'Date': ['31st-october-2019', '31st-october-2019', '31st-october-2019'],
                'Artist':['nts', 'nts', 'notnts'], 'Song': ['t1', 't2', 't3']}
    library = pd.DataFrame(data=library)

    def testCountByArtist(self):
        library_test = pd.Series([2, 1], index=['nts', 'notnts'])
        library_count = data_handler.countByArtist(self.library)
        self.assertTrue(pd.Series.equals(library_test, library_count))

    def testCountByDay(self):
        library_test = pd.Series([3], index=['31st-october-2019'])
        library_count = data_handler.countByDay(self.library)
        self.assertTrue(pd.Series.equals(library_test, library_count))

    def testDataFilter(self):
        library_test = pd.Series([2, 1], index=['nts', 'notnts'])
        library_test = data_handler.dataFilter(library_test, minCutOff=2)
        library_check = pd.Series([2], index=['nts'])
        self.assertTrue(pd.Series.equals(library_test, library_check))


class TestDataGrapher(unittest.TestCase):
    def testVolumePerDay(self):
        library_test = pd.Series([3, 2], index=['31st-october-2019', '30th-october-2019'])
        graph_response = data_grapher.barPlot(library_test)
        self.assertTrue(graph_response)


if __name__ == '__main__':
    unittest.main()
