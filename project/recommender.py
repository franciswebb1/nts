import os

import pandas as pd
import scipy.spatial as spatial
from sklearn.cluster import KMeans

import project.spotify_api as spotify_api
from resources.introduction_tracks import ARTISTS

CLUSTER = False


'''

Thoughts and plans.

    1. Introduce variance of tracks vs average as a variable, as well as % of unknown tracks. Ideally we would
       prefer to make recommendations on shows with a low variance and high % of known tracks.
       
       1.1 It might (maybe) also make sense to recommend shows with a high variance if the user has input a 
           show that also has a high variance - listening to diverse music => recommend diverse music

'''


out_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'output'))
resources_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'resources'))


class Recommender:

    def __init__(self, artist='', show=''):
        self.show_averages = pd.DataFrame()
        self.show_average_data = pd.DataFrame()
        self.artist = artist
        self.show = show
        self.centroids = []
        self.drop_columns = ['date', 'host', 'Url']

    def cluster_recommender(self):
        self.setup()
        self.k_means_clustering()
        if (self.artist != '') and (self.show != ''):
            self.kd_recommender_known_show()
        else:
            self.kd_recommender_user_track_ratings()

    def tree_recommender(self):
        self.setup()
        tree = spatial.KDTree(self.show_average_data)
        if (self.artist != '') and (self.show != ''):
            show_of_interest = self.show_averages[self.show_averages['Url'] == self.show]
            known_show = True
        else:
            # sur = StaticUserRecommendation()
            # user_preferences = sur.main()

            dur = DynamicUserRecommendation()
            show_of_interest = dur.main()
            known_show = False

        self.nearest_neighbours(tree=tree, show=show_of_interest, cluster=self.show_averages, known_show=known_show)

    def setup(self):
        self.show_averages = pd.read_csv(f'{out_path}/combined_shows_features_with_urls_3.csv')
        self.show_averages.dropna(inplace=True)
        self.show_average_data = self.show_averages.drop(self.drop_columns, axis=1)

    def k_means_clustering(self):
        kmeans_model = KMeans(n_clusters=100, max_iter=10000).fit(self.show_average_data)
        self.show_averages['cluster'] = kmeans_model.labels_
        self.centroids = kmeans_model.cluster_centers_

    def kd_recommender_known_show(self):
        show_of_interest = self.show_averages[self.show_averages['Url'] == self.show]
        if show_of_interest.empty:
            print('Whoops - we were unable to find your show in our database. Maybe try another and make sure'
                  'it has a tracklist!')
            return 1
        cluster_value = show_of_interest['cluster'].item()
        cluster_rows = self.show_averages[self.show_averages['cluster'] == int(cluster_value)]

        tree = spatial.KDTree(cluster_rows.drop(columns=self.drop_columns))
        self.nearest_neighbours(tree=tree, show=show_of_interest, cluster=cluster_rows, known_show=True)

    def kd_recommender_user_track_ratings(self):
        #sur = StaticUserRecommendation()
        #user_preferences = sur.main()

        dur = DynamicUserRecommendation()
        user_preferences = dur.main()

        tree = spatial.KDTree(self.centroids)
        best_fit_centroid = tree.query(user_preferences, k=1)[1]

        cluster_rows = self.show_averages[self.show_averages['cluster'] == best_fit_centroid]

        tree = spatial.KDTree(cluster_rows.drop(columns=self.drop_columns))
        user_preferences['cluster'] = best_fit_centroid
        self.nearest_neighbours(tree=tree, show=user_preferences, cluster=cluster_rows, known_show=False)

    def nearest_neighbours(self, tree, show, cluster, known_show):
        if known_show:
            nearest_neighbours = tree.query(show.drop(columns=self.drop_columns), k=5)
            nn_indexes = nearest_neighbours[1][0]
        else:
            try:
                nearest_neighbours = tree.query(show, k=5)
            except:
                nearest_neighbours = tree.query(show, k=len(cluster))
            nn_indexes = nearest_neighbours[1]
        if known_show:
            print(f'You enjoyed {self.artist} on {self.show}.')
            print('We think you might also enjoy:\n')
        else:
            print('Here are some shows we think you might enjoy')
        for i in range(len(nn_indexes)):
            if known_show:
                try:
                    if nearest_neighbours[0][0][i] != 0:
                        nearby_host = cluster.iloc[nearest_neighbours[1][0][i]]['host']
                        nearby_url = cluster.iloc[nearest_neighbours[1][0][i]]['Url']
                        print(f'    {nearby_host}, particularly episode: {nearby_url}')
                except Exception as e:
                    print('Index out out of bounds error (the cluster is too small!)')
                    return 1
            else:
                try:
                    nearby_host = cluster.iloc[nearest_neighbours[1][i]]['host']
                    nearby_url = cluster.iloc[nearest_neighbours[1][i]]['Url']
                    print(f'    {nearby_host}, particularly episode: {nearby_url}')
                except Exception as e:
                    print('Index out out of bounds error (the cluster is too small!)')
                    return 1


class StaticUserRecommendation:
    def __init__(self):
        self.cp = spotify_api.CreatePlaylist()
        self.artist_features = dict()
        self.user_artist_preferences = dict()
        self.index = ["acousticness", "danceability", "energy", "instrumentalness", "key", "liveness", "loudness",
                      "mode", "speechiness", "tempo", "valence"]
        self.user_preferences = pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], index=self.index)
        self.total_rating = 0

    def main(self):
        for artist in ARTISTS:
            track_ids = self.get_top_tracks_from_artist(artist_id=ARTISTS[artist][0])
            features = self.cp.get_show_average_data(uris=track_ids)
            features = features[self.index].mean()
            self.artist_features[artist] = features
        self.get_user_input()
        for artist in self.user_artist_preferences:
            self.user_preferences = self.user_artist_preferences[artist].add(self.user_preferences, fill_value=0)
        self.user_preferences = self.user_preferences / self.total_rating
        return self.user_preferences

    def get_top_tracks_from_artist(self, artist_id):
        return self.cp.get_top_tracks_from_artist(artist_id=artist_id)

    def get_user_input(self):
        for artist in ARTISTS:
            entry = False
            while not entry:
                print(f'Please give {artist} a rating from 1-10 (0 to pass)')
                rating = input('--> ')
                try:
                    rating = int(rating)
                    if (rating > 0) & (rating < 11):
                        self.user_artist_preferences[artist] = self.artist_features[artist]*rating
                        self.total_rating += rating
                        entry = True
                    elif rating == 0:
                        entry = True
                    else:
                        print('Your entry was out of range, please try again.')
                except:
                    print('Make sure to enter an integer between 1 and 10!')


class DynamicUserRecommendation:
    def __init__(self):
        self.cp = spotify_api.CreatePlaylist()
        self.artist_features = dict()
        self.user_artist_preferences = dict()
        self.index = ["acousticness", "danceability", "energy", "instrumentalness", "key", "liveness", "loudness",
                      "mode", "speechiness", "tempo", "valence"]
        self.user_preferences = pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], index=self.index)
        self.total_rating = 0
        self.artist_features = pd.read_csv(f'{resources_path}/artist_features.csv')
        self.artist_data = self.artist_features.drop('artist', axis=1)
        self.initial_artists = dict()
        self.secondary_artists = dict()

    def main(self):
        self.get_starting_artists()
        self.get_user_input(first_entry=True)
        for artist in self.user_artist_preferences:
            self.user_preferences = self.user_artist_preferences[artist][self.index].add(self.user_preferences, fill_value=0)

        # Now, lets redo our user preferences but taking only the closest 8 artists from our new centre

        tree = spatial.KDTree(pd.DataFrame(pd.DataFrame(self.artist_data)))
        nearest_neighbours = tree.query(self.user_preferences, k=8)

        self.secondary_artists = self.artist_features.iloc[nearest_neighbours[1]]
        self.secondary_artists = self.secondary_artists['artist'].values

        self.get_user_input(first_entry=False)

        for artist in self.user_artist_preferences:
            if artist not in self.initial_artists:
                if len(self.user_preferences) == 1:
                    self.user_preferences = self.user_artist_preferences[artist][self.index].iloc[0].add(
                        self.user_preferences.iloc[0])
                else:
                    self.user_preferences = self.user_artist_preferences[artist][self.index].iloc[0].add(
                        self.user_preferences)
        self.user_preferences = self.user_preferences / self.total_rating

        return self.user_preferences

    def get_starting_artists(self):
        kmeans_model = KMeans(n_clusters=3, max_iter=1000).fit(self.artist_data)
        centroids = kmeans_model.cluster_centers_
        self.artist_features['cluster'] = kmeans_model.labels_
        tree = spatial.KDTree(self.artist_data)
        for i in range(len(centroids)):
            nearest_neighbours = tree.query(centroids[i], k=2)
            index_1 = nearest_neighbours[1][0]
            index_2 = nearest_neighbours[1][1]
            self.initial_artists[self.artist_features.iloc[index_1]['artist']] = self.artist_features.iloc[index_1]
            self.initial_artists[self.artist_features.iloc[index_2]['artist']] = self.artist_features.iloc[index_2]

    def get_user_input(self, first_entry=True):
        if first_entry:
            artists = self.initial_artists
        else:
            artists = dict()
            for i in self.secondary_artists:
                if i not in self.initial_artists:
                    artists[i] = pd.DataFrame(self.artist_features[self.artist_features['artist'] == i]).drop('artist', axis=1)

        for artist in artists:
            entry = False
            while not entry:
                print(f'Please give {artist} a rating from 1-10 (0 to pass)')
                rating = input('--> ')
                try:
                    rating = int(rating)
                    if (rating > 0) & (rating < 11):
                        if first_entry:
                            self.user_artist_preferences[artist] = self.initial_artists[artist]*rating
                        else:
                            self.user_artist_preferences[artist] = artists[artist]*rating
                        self.total_rating += rating
                        entry = True
                    elif rating == 0:
                        entry = True
                    else:
                        print('Your entry was out of range, please try again.')
                except:
                    print('Make sure to enter an integer between 1 and 10!')


def start():
    print('Would you prefer to get recommendations based on...')
    print("     an NTS show you have enjoyed? (n/N)")
    print("     your artist preferences? (a/A)\n")
    user_input = input('--> ').lower()
    if (user_input == 'n') | (user_input == 'nts'):
        print('Enter the name of the host you are interested in:')
        host = input('--> ')
        print('Enter the link for a show you enjoyed: (you might need to add a space at the end)')
        show = input('--> ')
        if show[-1] == ' ':
            show = show[:-1]
        if show[-1] != '/':
            show = show + '/'
        rec = Recommender(artist=host, show=show)
        rec.tree_recommender()
    elif (user_input == 'a') | (user_input == 'artist'):
        rec = Recommender()
        rec.tree_recommender()
    else:
        print("Whoops - invalid entry!")
        start()


if __name__ == '__main__':
    start()
