import matplotlib.pyplot as plt


def barPlot(data):
    try:
        data.plot.bar()
        plt.show()
        return True
    except Exception as e:
        print(e)
        return False


def scatterPlot(data, show_name): # used when plotting features against date e.g. danceability,loudness, etc..
    columns = data.columns[1:]
    for col in columns:
        fig, ax = plt.subplots()
        ax.scatter(x=data['Date'], y=data[col], alpha=0.5)
        ax.set_xlabel('Date', fontsize=15)
        ax.set_ylabel(f'{col}', fontsize=15)
        ax.set_title(f'{show_name} {col}')

        ax.grid(True)
        fig.tight_layout()

        plt.show()
