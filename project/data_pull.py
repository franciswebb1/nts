import json
import pandas as pd
import requests
from bs4 import BeautifulSoup


def connectBS(url):
    try:
        r = requests.get(url)
        parsed_html = BeautifulSoup(r.text)
    except:
        parsed_html = "Connection error - 111"
    return parsed_html


def findTracklist(ph):
    tracklist = ph.find("div", {"class": "shows-listings tracklist"})
    return tracklist


def findSongs(tracklist, date, url):
    tracks = pd.DataFrame({'Date': [], 'Artist': [], 'Song': [], 'Url': []})
    for track in tracklist.find_all("li", {"class": "track"}):
        try:
            artist = track.find("span", {"class": "track__artist track__artist--mobile"}).text
            song = track.find("span", {"class": "track__title"}).text
            tracks.loc[len(tracks)] = [date, artist, song, url]
        except Exception as e:
            print(f'Error finding track name or artist. Show: {date} \n {e} ')
    return tracks


def dateSplit(url):
    url = url.split('-')
    months = {'jan': 'january', 'feb': 'february', 'mar': 'march', 'apr': 'april', 'jun': 'june', 'jul': 'july',
              'aug': 'august', 'sep': 'september', 'oct': 'october', 'nov': 'november', 'dec': 'december'}
    if url[-2] in months.keys():
        url[-2] = months[url[-2]]
    return url[-3] + '-' + url[-2] + '-' + url[-1]


def find_show_urls(host_name):
    urls = []
    i = 0
    limit_reached = False
    while not limit_reached:
        url = f'https://www.nts.live/api/v2/shows/{host_name}/episodes?offset={i}&limit=12'
        html = connectBS(url)
        if html == "Connection error - 111":
            limit_reached = True
        elif '"error":"Forbidden: The requested offset is not allowed."' in html.find("p").text:
            limit_reached = True
        else:
            j = 0
            while j < len(json.loads(str(html.text))['results']):
                show_url = json.loads(str(html.text))['results'][j]['links'][-1]['href'].replace("/api/v2", "")
                show_url = show_url.replace("tracklist", "")
                urls.append(show_url)
                j += 1

        i += 12
    return urls


def find_host_urls():
    urls = ['host_url']
    i = 0
    limit_reached = False
    while not limit_reached:
        host_list_url = f'https://www.nts.live/shows/page/{i}?ajax=true'
        html = connectBS(url=host_list_url)
        host_anchor_tags = html.find_all('a', 'grid-item nts-app')
        if not host_anchor_tags:
            limit_reached = True
        else:
            j = 0
            while j < len(host_anchor_tags):
                if host_anchor_tags[j]['href'] != 'guests':
                    urls.append(host_anchor_tags[j]['href'])
                j += 1
        i += 1
    return urls

