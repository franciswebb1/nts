import os

import pandas as pd

from project.spotify_api import CreatePlaylist


out_path = os.path.abspath(os.path.join(os.path.dirname( __file__ )))


# This is meant to be songs people are guaranteed to have

ARTISTS = {'Fleetwood Mac': ['08GQAI4eElDnROBrJRGE0X'],
           'Wu-Tang Clan': ['34EP7KEpOjXcM2TCat1ISk'],
           'Gorillaz': ['3AA28KZvwAUcZuOKwyblJQ'],
           'The xx': ['3iOvXCl6edW5Um0fXEBRXy'],
           'Ragz Orignale': ['0yw2O9rh7YQd5MqEHOFGzW'],
           'Yaeji': ['2RqrWplViWHSGLzlhmDcbt'],
           'Metallica': ['2ye2Wgw4gimLv2eAKyk1NB'],
           'Frank Ocean': ['2h93pZq0e7k5yf4dywlkpM'],
           'Stevie Wonder': ['7guDJrEfX3qb6FEbdPA5qi'],
           'Amy Winehouse': ['6Q192DXotxtaysaqNPy5yR'],
           'Lou Reed': ['42TFhl7WlMRXiNqzSrnzPL'],
           'Rolling Stones': ['22bE4uQ6baNwSHPVcDxLCe'],
           'Jon Hopkins': ['7yxi31szvlbwvKq9dYOmFI'],
           'Ariane Grande': ['66CXWjxzNUsdJxJ2JdwvnR'],
           'Muddy Waters': ['4y6J8jwRAwO4dssiSmN91R'],
           'Dolly Parton': ['32vWCbZh0xZ4o9gkz4PsEU'],
           'Daft Punk': ['4tZwfgrHOc3mvqYlEYSvVi'],
           'Bob Dylan': ['74ASZWbe4lXaubB36ztrGX'],
           'Nas': ['20qISvAhX20dpIbOOzGK3q'],
           'Billie Holiday': ['1YzCsTRb22dQkh9lghPIrp'],
           'Taylor Swift': ['06HL4z0CvFAxyc27GXpf02'],
           'Prince': ['5a2EaR3hamoenG9rDuVn8j'],
           'Bob Marley': ['2QsynagSdAqZj3U9HgDzjD'],
           'Headie One': ['6UCQYrcJ6wab6gnQ89OJFh'],
           'Ross from Friends': ['1Ma3pJzPIrAyYPNRkp3SUF']
           }


ARTISTS_SMALL = {'Fleetwood Mac': ['08GQAI4eElDnROBrJRGE0X'],
                 'Wu-Tang Clan': ['34EP7KEpOjXcM2TCat1ISk'],
                 'Gorillaz': ['3AA28KZvwAUcZuOKwyblJQ']}


ARTISTS_BIG = {'Fleetwood Mac': ['08GQAI4eElDnROBrJRGE0X'],
               'Taylor Swift': ['06HL4z0CvFAxyc27GXpf02'],
               'BICEP': ['73A3bLnfnz5BoQjb4gNCga'],
               'Wu-Tang Clan': ['34EP7KEpOjXcM2TCat1ISk'],
               'Gorillaz': ['3AA28KZvwAUcZuOKwyblJQ'],
               'The xx': ['3iOvXCl6edW5Um0fXEBRXy'],
               'Ragz Orignale': ['0yw2O9rh7YQd5MqEHOFGzW'],
               'Yaeji': ['2RqrWplViWHSGLzlhmDcbt'],
               'Metallica': ['2ye2Wgw4gimLv2eAKyk1NB'],
               'Frank Ocean': ['2h93pZq0e7k5yf4dywlkpM'],
               'Stevie Wonder': ['7guDJrEfX3qb6FEbdPA5qi'],
               'Amy Winehouse': ['6Q192DXotxtaysaqNPy5yR'],
               'Lou Reed': ['42TFhl7WlMRXiNqzSrnzPL'],
               'Rolling Stones': ['22bE4uQ6baNwSHPVcDxLCe'],
               'Jon Hopkins': ['7yxi31szvlbwvKq9dYOmFI'],
               'Ariane Grande': ['66CXWjxzNUsdJxJ2JdwvnR'],
               'Muddy Waters': ['4y6J8jwRAwO4dssiSmN91R'],
               'Dolly Parton': ['32vWCbZh0xZ4o9gkz4PsEU'],
               'Daft Punk': ['4tZwfgrHOc3mvqYlEYSvVi'],
               'Bob Dylan': ['74ASZWbe4lXaubB36ztrGX'],
               'Nas': ['20qISvAhX20dpIbOOzGK3q'],
               'Billie Holiday': ['1YzCsTRb22dQkh9lghPIrp'],
               'Drake': ['3TVXtAsR1Inumwj472S9r4'],
               'Prince': ['5a2EaR3hamoenG9rDuVn8j'],
               'Bob Marley': ['2QsynagSdAqZj3U9HgDzjD'],
               'Headie One': ['6UCQYrcJ6wab6gnQ89OJFh'],
               'Ross from Friends': ['1Ma3pJzPIrAyYPNRkp3SUF'],
               'Dusky': ['5gqoUf9vKKv96b1c0GBKwu'],
               'Casisdead': ['0n7CYdHaJm01NFXRhwbbKs'],
               'Dizzee Rascal': ['0gusqTJKxtU1UTmNRMHZcv'],
               'Giggs': ['3S0tlB4fE7ChxI2pWz8Xip'],
               'Arthur Russel': ['3iJJD5v7oIFUevW4N5w5cj'],
               'KAYTRANADA': ['6qgnBH6iDM91ipVXv28OMu'],
               'Rex Orange County': ['7pbDxGE6nQSZVfiFdq9lOL'],
               'Leonard Cohen': ['5l8VQNuIg0turYE1VtM9zV'],
               'Four Tet': ['7Eu1txygG6nJttLHbZdQOh'],
               'Van Morrisson': ['44NX2ffIYHr6D4n7RaZF7A'],
               'The Streets': ['4GvOygVQquMaPm8oAc0vXi'],
               'Loyle Carner': ['4oDjh8wNW5vDHyFRrDYC4k'],
               'Skream': ['2jbP92oFLWqPqogflK1wlW'],
               'Charlie XCX': ['25uiPmTg16RbhZWAqwLBy5'],
               'Joni Mitchel': ['5hW4L92KnC6dX9t7tYM4Ve'],
               'Womack and Womack': ['7qShKycqNUP0GLEiTENDVZ'],
               'Gill Scott Herron': ['0kEfub5RzlZOB2zGomqVSU'],
               'Prefab Sprout': ['4w3QqrcmBv8dasemwBXmxf'],
               'Tame Impala': ['5INjqkS1o8h1imAzPqGZBb'],
               'Foals': ['6FQqZYVfTNQ1pCqfkwVFEa'],
               'Kojey Radical': ['1HMhQzj2QXxR40zGDdaK6y'],
               'T2': ['4Q5MS10mWfrDyiXVeAAzVd'],
               'Lana Del Rey': ['00FQb4jTyendYWaN8pK0wa'],
               'James Blake': ['53KwLdlmrlCelAZMaLVZqU'],
               'Skepta': ['2p1fiYHYiXz9qi0JJyxBzN'],
               'Sylvan Esso': ['39vA9YljbnOApXKniLWBZv'],
               'Tokyo Prose': ['361kscBTEw82NTOFS8hq0D'],
               'Kolsch': ['2D9Oe8R9UhbMvFAsMJpXj0'],
               'Fatima Yamaha': ['7eZRt08LoDy0nfIS6OwyMP'],
               'Ejeca': ['0tSC9Vot7WlR1MsLBqQ9HX'],
               'Alan Fitzpatrick': ['40JyDxGqtYSowWYT2jaive'],
               'Chase and Status': ['3jNkaOXasoc7RsxdchvEVq'],
               'Denis Sulta': ['7cDu9zG1gVQrMdSGBAhzvn'],
               'Groove Armada': ['67tgMwUfnmqzYsNAtnP6YJ'],
               'Chrvches': ['3CjlHNtplJyTf9npxaPl5w'],
               'Majid Jordan': ['4HzKw8XcD0piJmDrrPRCYk'],
               'J Hus': ['2a0uxJgbvvIRI4GX8pYfcr'],
               'Tom Misch': ['1uiEZYehlNivdK3iQyAbye'],
               'Celeste': ['49HlOY4gkHqsYG9GCuhkcc'],
               'Nirvana': ['6olE6TJLqED3rqDCT0FyPh'],
               'SOPHIE': ['5a2w2tgpLwv26BYJf2qYwu'],
               'A.G. Cook': ['335TWGWGFan4vaacJzSiU8'],
               'Erol Alkan': ['3jQ8hpdQo3TCEnb5gmOtH5'],
               'Boyz Noize': ['62k5LKMhymqlDNo2DWOvvv']
               }


def show_recommendation_setup():
    cp = CreatePlaylist()
    columns = ["acousticness", "danceability", "energy", "instrumentalness", "key", "liveness", "loudness", "mode",
               "speechiness", "tempo", "valence"]
    artist_features = pd.DataFrame(columns=columns)
    for artist in ARTISTS_BIG:
        track_ids = cp.get_top_tracks_from_artist(artist_id=ARTISTS_BIG[artist][0])
        features = cp.get_show_average_data(uris=track_ids)
        features = features[columns].mean()
        artist_features.loc[artist] = features
    artist_features.reset_index(drop=False, inplace=True)
    artist_features.rename(columns={'index': 'artist'}, inplace=True)
    artist_features.to_csv(f'{out_path}/artist_features.csv', index=False)


if __name__ == '__main__':
    show_recommendation_setup()

